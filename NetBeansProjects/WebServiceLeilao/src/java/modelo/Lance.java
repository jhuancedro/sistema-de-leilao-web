package modelo;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Calendar;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Lance implements Serializable{
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    
//    @ManyToOne//(cascade = CascadeType.PERSIST)
//    private Leilao leilao;
    
//    @ManyToOne//(cascade = CascadeType.PERSIST)
//    private Usuario usuario;
    
    @ManyToOne(cascade = CascadeType.PERSIST)
    private Participacao participacao;
    
    @Column
    private float quantidade;
    
    @Column
    private boolean compra;
    
    @Column
    private Timestamp hora;
    
    public Lance(){}
    
    public Lance(Participacao p, float quantidade, boolean ehCompra){
//        this.usuario = u;
        this.participacao = p;
        this.quantidade = quantidade;
        this.compra = ehCompra;
        this.hora = new Timestamp(Calendar.getInstance().getTimeInMillis());
    }

//    public Usuario getUsuario() {
//        return usuario;
//    }
//
//    public void setUsuario(Usuario usuario) {
//        this.usuario = usuario;
//        usuario.adcionarLance(this);
//    }

    public float getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(float quantidade) {
        this.quantidade = quantidade;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Timestamp getHora() {
        return hora;
    }

    public void setHora(Timestamp hora) {
        this.hora = hora;
    }

    public boolean isCompra() {
        return compra;
    }

    public void setCompra(boolean compra) {
        this.compra = compra;
    }

//    public Leilao getLeilao() {
//        return leilao;
//    }
//
//    public void setLeilao(Leilao leilao) {
//        this.leilao = leilao;
//    }

    public Participacao getParticipacao() {
        return participacao;
    }

    public void setParticipacao(Participacao participacao) {
        this.participacao = participacao;
    }
}
