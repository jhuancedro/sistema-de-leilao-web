package modelo;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import webService.SessaoWeb;

@Entity
public class Usuario implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    
    @Column(length = 50)
    private String email;
    
    @Column(length = 30)
    private String nome;
    
//    @Column(length = 11)    
//    private String cpf;
    
    private byte[] senha;
    
    private Timestamp dataCadastro;
    private Timestamp ultimoAcesso;
    
//    @OneToMany(mappedBy = "usuario"/*, cascade = CascadeType.PERSIST*/)
//    private List<Lance> lances;
    
//    @ManyToMany(mappedBy = "usuarios")
//    private List<Leilao> leiloes;
    
    @OneToMany(mappedBy = "usuario", cascade = CascadeType.ALL)
    private List<Participacao> participacoes;    
    
    @OneToMany(mappedBy = "usuario", cascade = CascadeType.ALL)
    private List<SessaoWeb> sessoes;

    public String getNome() {
        return nome;
    }
    
    public Usuario(){
//        lances = new ArrayList<>();
        sessoes = new ArrayList<>();
        participacoes = new ArrayList<>();
        dataCadastro = new Timestamp(Calendar.getInstance().getTimeInMillis());
    }
    
    public Usuario(String nome, String email){
        this.nome = nome;
        this.email = email;
        participacoes = new ArrayList<>();
        sessoes = new ArrayList<>();
//        lances = new ArrayList<>();
        dataCadastro = new Timestamp(Calendar.getInstance().getTimeInMillis());
    }
    
    /**  SESSAO É INCLUÍDA AO SER CRIADA
    public void incuirSessao(SessaoWeb sessaoWeb){
        this.sessoes.add(sessaoWeb);
    }
    
    public void incuirSessao(String chave){
        incuirSessao(new SessaoWeb(this));
    }
    
    public void removerSessao(SessaoWeb sessaoWeb){
        this.getSessoes().remove(sessaoWeb);
    }
    
    public void removerSessao(String chave){
        for (SessaoWeb sessaoWeb : this.getSessoes()) {
            if (sessaoWeb.getChave().equals(chave)){
                this.removerSessao(sessaoWeb);
                break;
            }
        }
    }
    */
    
//    public void darLance(Lance lance){
//        getLances().add(lance);
//    }
    
    public Participacao participarLeilao(Leilao leilao, float estoqueInicial){
        Participacao novaParticipacao = new Participacao(this, leilao, estoqueInicial);
        this.participacoes.add(novaParticipacao);
        leilao.adicionarParticipacao(novaParticipacao);
        return novaParticipacao;
    }
    
    public boolean equals(Usuario usuario){
        return /*cpf.equals(usuario.getCPF()) &&*/ email.equals(usuario.getEmail());
    }
    
    public void comprar(Leilao leilao, float quantidade, float preco){
        this.darLance(true, leilao, quantidade, preco);
    }
    
    public void vender(Leilao leilao, float quantidade, float preco){
        this.darLance(false, leilao, quantidade, preco);
    }
    
    public void darLance(boolean ehCompra, Leilao leilao, float quantidade, float preco){
        Participacao participacao = buscarParticipacao(leilao);
        if(participacao != null)
            participacao.adicionarLance(new Lance(participacao, quantidade, ehCompra));
    }
    
    public Participacao buscarParticipacao(Leilao leilao){
        for (Participacao p : participacoes) {
            if(p.getLeilao() == leilao)
                return p;
        }  
        return null;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    /*public String getCPF() {
    return cpf;
    }
    
    public void setCPF(String cpf) {
    this.cpf = cpf;
    }*/

    public byte[] getSenha() {
        return senha;
    }

    public void setSenha(byte[] senha) {
        this.senha = senha;
    }

    public Timestamp getDataCadastro() {
        return dataCadastro;
    }

    public void setDataCadastro(Timestamp dataCadastro) {
        this.dataCadastro = dataCadastro;
    }

    public Timestamp getUltimoAcesso() {
        return ultimoAcesso;
    }

    public void setUltimoAcesso(Timestamp ultimoAcesso) {
        this.ultimoAcesso = ultimoAcesso;
    }
//
//    public List<Lance> getLances() {
//        return lances;
//    }
//
//    public void setLances(List<Lance> lances) {
//        this.lances = lances;
//    }

//    public List<Leilao> getLeiloes() {
//        return leiloes;
//    }
//
//    public void setLeiloes(List<Leilao> leiloes) {
//        this.leiloes = leiloes;
//    }

    public List<SessaoWeb> getSessoes() {
        return sessoes;
    }

    public void setSessoes(List<SessaoWeb> sessoes) {
        this.sessoes = sessoes;
    }

    public List<Participacao> getParticipacoes() {
        return participacoes;
    }

    public void setParticipacoes(List<Participacao> participacoes) {
        this.participacoes = participacoes;
    }
}
