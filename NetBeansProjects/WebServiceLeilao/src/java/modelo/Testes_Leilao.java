package modelo;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.naming.NamingException;
import javax.persistence.EntityManager;
import persistencia.GenericDAO;
import persistencia.PersistenceUtil;

public class Testes_Leilao {
    public static void main(String[] args) throws SQLException, NamingException {
        GenericDAO<Usuario> usuarioDAO = new GenericDAO<>(Usuario.class);
        EntityManager manager = PersistenceUtil.getEntityManager();

        Leilao leilao = new Leilao();
        leilao.iniciar(60*1000);

        List<Usuario> usuarios = new ArrayList<>();
        usuarios.add( new Usuario("Adalberto", "adalberto@email.com") );
        usuarios.add( new Usuario("Algoz", "algoz@email.com") );
//        usuarios.add( new Usuario("Andre", "andre@email.com") );
//        usuarios.add( new Usuario("Antonia", "antonia@email.com") );
//        usuarios.add( new Usuario("Arnaldo", "arnaldo@email.com") );
//        usuarios.add( new Usuario("Atila", "atila@email.com") );
//        usuarios.add( new Usuario("Jhuan", "jhuancedro@gmail.com") );
//            usuarios.add( new Usuario("Lorran", "lorran.sutter@engenharia.ufjf.br") );

        for (Usuario usuario : usuarios)
            usuario.participarLeilao(leilao, 500);

        usuarios.get(1).comprar(leilao, 500.0f, 1.0f);
//        usuarios.get(4).comprar(leilao,  30.0f, 1.0f);
//        usuarios.get(3).comprar(leilao, 320.0f, 1.0f);
//        usuarios.get(4).comprar(leilao, 200.0f, 1.0f);
//        usuarios.get(2).vender(leilao, 100.0f, 2.0f);
//        usuarios.get(2).vender(leilao,  80.0f, 1.0f);
//        usuarios.get(5).vender(leilao, 150.0f, 2.3f);
//        usuarios.get(1).vender(leilao, 100.0f, 1.7f);
        usuarios.get(0).vender(leilao, 150.0f, 0.9f);

            for (Usuario usuario : usuarios) 
                usuarioDAO.update(usuario, manager);

//        usuarioDAO.update(usuarios, manager);
    }
}
