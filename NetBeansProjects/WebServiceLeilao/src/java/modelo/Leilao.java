package modelo;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Leilao implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    
    @Column(length = 36)
    private String chave;
    
    private Timestamp dataInicio;
    private long duracaoEmMilissegundos;
//    private int quantidade;
//    private float precoInicial, precoAtual;
    
    @ManyToOne(cascade = CascadeType.ALL)
    private Usuario administrador;
    
//    @ManyToMany//(cascade = CascadeType.PERSIST)
//    private List<Usuario> usuarios;
    
    @OneToMany(cascade = CascadeType.ALL)
    private List<Participacao> participacoes;
    
//    @OneToMany(mappedBy = "leilao")
//    private List<Lance> lances;

    public Leilao(){
//        this.usuarios = new ArrayList<>();
        this.participacoes = new ArrayList<>();
//        this.lances = new ArrayList<>();
        this.chave = gerarChave();
    }
    
    public Leilao(Timestamp dataInicio, long duracaoEmMilissegundos){  
//        this.usuarios = new ArrayList<>();
        this.participacoes = new ArrayList<>();
//        this.lances = new ArrayList<>();
        this.chave = gerarChave();
        
        this.dataInicio = dataInicio;
        this.duracaoEmMilissegundos = duracaoEmMilissegundos;
    }
    
    public void iniciar(long duracaoEmMilissegundos){
        this.dataInicio = new Timestamp(Calendar.getInstance().getTimeInMillis()); 
        this.duracaoEmMilissegundos = duracaoEmMilissegundos;
    }
    
    public void setDuracao(int dias, int horas, int minutos){
        this.duracaoEmMilissegundos = 1000*60*(minutos + 60*horas + 24*dias);
    }
    
    public void adicionarParticipacao(Participacao participacao){
        this.participacoes.add(participacao);
    }
    
//    public void adicionarLance(Lance lance){
//        getLances().add(lance);
//    }
    
    public boolean ehAdministrador(Usuario usuario){
        return usuario.equals(administrador);
    }
    
    private String gerarChave(){
        return String.valueOf(UUID.randomUUID());
    }
    
    public float getPrecoEquilibrio(){
        return 10.0f;
    }
    
    public Participacao buscarParticipacao(String email){
        for (Participacao participacao : participacoes) {
            if(participacao.getUsuario().getEmail().equals(email))
                return participacao;
        }
        return null;
    }
    
    public Usuario buscarUsuarioParticipante(String email){
        for (Participacao participacao : participacoes) {
            if(participacao.getUsuario().getEmail().equals(email))
                return participacao.getUsuario();
        }
        return null;
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Timestamp getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(Timestamp dataInicio) {
        this.dataInicio = dataInicio;
    }

//    public List<Usuario> getUsuarios() {
//        return usuarios;
//    }
//
//    public void setUsuarios(List<Usuario> usuarios) {
//        this.usuarios = usuarios;
//    }

//    public List<Lance> getLances() {
//        return lances;
//    }
//
//    public void setLances(List<Lance> lances) {
//        this.lances = lances;
//    }

    public Usuario getAdministrador() {
        return administrador;
    }

    public void setAdministrador(Usuario administrador) {
        this.administrador = administrador;
    }

    public String getChave() {
        return chave;
    }

    public void setChave(String chave) {
        this.chave = chave;
    }

    public long getDuracaoEmMilissegundos() {
        return duracaoEmMilissegundos;
    }

    public void setDuracaoEmMilissegundos(long duracaoEmMilissegundos) {
        this.duracaoEmMilissegundos = duracaoEmMilissegundos;
    }

//    public int getQuantidade() {
//        return quantidade;
//    }
//
//    public void setQuantidade(int quantidade) {
//        this.quantidade = quantidade;
//    }
//
//    public float getPrecoAtual() {
//        return precoAtual;
//    }
//
//    public void setPrecoAtual(float precoAtual) {
//        this.precoAtual = precoAtual;
//    }
//
//    public float getPrecoInicial() {
//        return precoInicial;
//    }
//
//    public void setPrecoInicial(float precoInicial) {
//        this.precoInicial = precoInicial;
//    }

    public List<Participacao> getParticipacoes() {
        return participacoes;
    }

    public void setParticipacoes(List<Participacao> participacoes) {
        this.participacoes = participacoes;
    }

}
