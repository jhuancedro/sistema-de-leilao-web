package controller;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.naming.NamingException;
import javax.persistence.EntityManager;
import modelo.Usuario;
import persistencia.GenericDAO;
import persistencia.PersistenceUtil;
import utilitarios.Criptografia;
import webService.SessaoWeb;

@ManagedBean(name="Login")
@RequestScoped
public class LoginController {
//    private static final String URL_WEBSERVICE = "http://localhost:8080/Teste_Cliente_Leilao/TestarLances?Tester";
    private String email, senha, chave;
    private Usuario usuario;

    public LoginController() {
    }    

    public void logar() throws NamingException, SQLException, NoSuchAlgorithmException, UnsupportedEncodingException{
        GenericDAO<Usuario> usuarioDAO = new GenericDAO<>(Usuario.class);
        ///     DESFAZER ESTE USO DE SessaoWeb APÓS NORMALIZAR WEBSERVICE   ///
        GenericDAO<SessaoWeb> sessaoWebDAO = new GenericDAO<>(SessaoWeb.class);
        EntityManager manager = PersistenceUtil.getEntityManager();
        
        Usuario usuario = usuarioDAO.listOnce("email", email, manager);
        if (usuario != null && Criptografia.comparar(senha, usuario.getSenha())){
            SessaoWeb sessaoWeb = new SessaoWeb(usuario);
            System.out.println("Login efetuado com sucesso às " + sessaoWeb.getHoraLogin().toString());
            this.chave = sessaoWeb.getChaveDaSessao();
            sessaoWebDAO.update(sessaoWeb, manager);
        }else
            System.out.println("Nao foi possivel realizar o login!");
    }
    
//    public void logar() throws UnsupportedEncodingException, NoSuchAlgorithmException{
//        Session hibernateSession = NovoHibernateUtil.getSessionFactory().openSession();
//        Transaction transaction = hibernateSession.beginTransaction();
//            Usuario usuario = WebServiceLeilao.buscarUsuario(email, hibernateSession, transaction);
//            
//            if (usuario != null && Arrays.equals(usuario.getSenha(), WebServiceLeilao.criptografia(senha) )){
//                SessaoWeb sessaoWeb = new SessaoWeb(usuario);
//                hibernateSession.save(sessaoWeb);
//                transaction.commit();
//                System.out.println("Login efetuado com sucesso às " + sessaoWeb.getHoraLogin().toString());
//                this.chave = sessaoWeb.getChaveDaSessao();
//            }else
//                System.out.println("Nao foi possivel realizar o login!");
//        hibernateSession.close();
//    }
    
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }
    
    
    public void cadastrar(){
    }
    
    public void solicitarSenha(){
    
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
}
