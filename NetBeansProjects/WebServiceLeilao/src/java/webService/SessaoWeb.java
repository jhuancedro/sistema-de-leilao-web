package webService;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.UUID;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import modelo.Usuario;

@Entity
public class SessaoWeb implements Serializable {
    // Tempo da sessão em milissegundos
    private static final long session_time = 10*(60*1000);
    
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    
    @ManyToOne( cascade = CascadeType.ALL )
    private Usuario usuario;
    
    @Column(length = 36)
    private String chaveDaSessao;
    
    private Timestamp horaLogin;
    
    private Timestamp horaUltimoAcesso;
    
    public SessaoWeb(){
        this.horaLogin = new Timestamp(Calendar.getInstance().getTimeInMillis());
        this.horaUltimoAcesso = new Timestamp(Calendar.getInstance().getTimeInMillis());   
    }
    
    public SessaoWeb(Usuario usuario){
        this.usuario = usuario;
        this.chaveDaSessao = gerarChave();
        this.horaLogin = new Timestamp(Calendar.getInstance().getTimeInMillis());
        this.horaUltimoAcesso = new Timestamp(Calendar.getInstance().getTimeInMillis());
        
        usuario.setUltimoAcesso(horaUltimoAcesso);
        usuario.getSessoes().add(this);
    }
    
    private String gerarChave(){
        return String.valueOf(UUID.randomUUID());
    }
    
    public boolean estaLogado(){
        return (Calendar.getInstance().getTimeInMillis()
                - this.getHoraUltimoAcesso().getTime()) 
                < session_time;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public String getChaveDaSessao() {
        return chaveDaSessao;
    }

    public void setChaveDaSessao(String chaveDaSessao) {
        this.chaveDaSessao = chaveDaSessao;
    }

    public Timestamp getHoraLogin() {
        return horaLogin;
    }

    public void setHoraLogin(Timestamp horaLogin) {
        this.horaLogin = horaLogin;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Timestamp getHoraUltimoAcesso() {
        return horaUltimoAcesso;
    }

    public void setHoraUltimoAcesso(Timestamp horaUltimoAcesso) {
        this.horaUltimoAcesso = horaUltimoAcesso;
    }
}
