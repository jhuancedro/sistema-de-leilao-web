package webService;

import utilitarios.JavaMailApp;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Calendar;
import javax.faces.bean.ManagedBean;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.mail.MessagingException;
import javax.naming.NamingException;
import javax.persistence.EntityManager;
import modelo.Leilao;
import persistencia.NovoHibernateUtil;
import modelo.Usuario;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import persistencia.GenericDAO;
import persistencia.PersistenceUtil;
import utilitarios.Criptografia;

//@Stateless
@ManagedBean(name="WEB")
@WebService(serviceName = "TestarLances")
public class WebServiceLeilao {
//    @Resource
//    private WebServiceContext wsContext;
    
    @WebMethod(operationName = "pontape_inicial")
    public void testeInicial() throws SQLException, NamingException{
        modelo.Testes_Leilao.main(null);
    }
    
    public static Usuario buscarUsuario(String email, Session hibernateSession, 
            Transaction transaction){
        Criteria busca = hibernateSession.createCriteria(Usuario.class);
        busca.add(Restrictions.eq("email", email));
        Object obj = busca.uniqueResult();
        
        return (obj != null)? (Usuario) obj : null;
    }
    
    public static SessaoWeb buscarSessao(String chave, Session hibernateSession, 
            Transaction transaction){
        Criteria busca = hibernateSession.createCriteria(SessaoWeb.class);
        busca.add(Restrictions.eq("chave", chave));
        Object obj = busca.uniqueResult();
        
        return (obj != null)? (SessaoWeb) obj : null;
    }
    
    public static Leilao buscarLeilao(String chave, Session hibernateSession, 
            Transaction transaction){
        Criteria busca = hibernateSession.createCriteria(Leilao.class);
        busca.add(Restrictions.eq("chave", chave));
        Object obj = busca.uniqueResult();
        
        return (obj != null)? (Leilao) obj : null;
    }
    
    @WebMethod(operationName = "Cadastrar")
    public void cadastrar(
            @WebParam(name = "Nome") String nome,
            /*@WebParam(name = "CPF") String cpf,*/
            @WebParam(name = "email") String email){

//        Session hibernateSession = NovoHibernateUtil.getSessionFactory().openSession();
//        Transaction transaction = hibernateSession.beginTransaction();
        try{
            GenericDAO<Usuario> usuarioDAO = new GenericDAO<>(Usuario.class);
            EntityManager manager = PersistenceUtil.getEntityManager();

    //        Usuario usuario = buscarUsuario(email, hibernateSession, transaction);
            Usuario usuario = usuarioDAO.listOnce("email", email, manager);
            if( usuario != null)
                System.out.println("Usuário já cadastrado!");       
            else{
                /*
                if(!CPF.isCPF(cpf))
                    System.out.println("CPF informado é inváido");
                else{*/
                    Usuario novoUsuario = new Usuario(nome, email);
    //                novoUsuario.setCPF(cpf);
//                    try{
                        String novaSenha = Criptografia.gerarSenha();
                        String txt = "Olá, " + novoUsuario.getNome();
                        txt+= "\n\nSeja bem-vindo ao Sistema de Leilões!\nAqui está a sua primeira senha:\n";
                        txt+= "\t" + novaSenha;
                        JavaMailApp.enviarEmail(novoUsuario.getEmail(), "Sistema de Leilao - Cadastro", txt);

                        novoUsuario.setSenha(Criptografia.criptografia(novaSenha));
                        usuarioDAO.update(novoUsuario, manager);
                        System.out.println("Nova senha enviada por email.");
//                    }
//                    catch(MessagingException e){
//                        System.out.println(e.getMessage());
//                        System.out.println("Nao foi possivel eftuar o cadastro!");
//                    }
    //            }
            }
        }
        catch(NamingException | SQLException | MessagingException | NoSuchAlgorithmException | UnsupportedEncodingException e){
            System.out.println(e.getMessage());
            System.out.println("Nao foi possivel eftuar o cadastro!");
        }
    }
    
    @WebMethod(operationName = "Criar_Leilao")
    public String criarLeilao(
            @WebParam(name = "chave_da_sessao") String chaveSessao, 
            @WebParam(name = "duracaoEmMilissegundos") long duracaoEmMilissegundos){
        Session hibernateSession = NovoHibernateUtil.getSessionFactory().openSession();
        Transaction transaction = hibernateSession.beginTransaction();
        Leilao leilao = new Leilao();
            SessaoWeb sessaoWeb = buscarSessao(chaveSessao, hibernateSession, transaction);
            if (sessaoWeb == null)
                System.out.println("Chave da sessão não encontrada!");
            else {
                leilao.setAdministrador(sessaoWeb.getUsuario());
                leilao.iniciar(duracaoEmMilissegundos);
                hibernateSession.save(leilao);
            }
        
        transaction.commit();
        hibernateSession.close();
        
        return leilao.getChave();
    }
    
    
    @WebMethod(operationName = "Dar_lance")
    public void darLance(@WebParam(name = "chave_da_sessao") String chaveSessao,
            @WebParam(name = "chave_do_leilao") String chaveLeilao,
            @WebParam float quantidade, @WebParam float valor, @WebParam String tipo){
        Session hibernateSession = NovoHibernateUtil.getSessionFactory().openSession();
        Transaction transaction = hibernateSession.beginTransaction();
        
            SessaoWeb sessaoWeb = buscarSessao(chaveSessao, hibernateSession, transaction);
            if (sessaoWeb == null)
                System.out.println("Sessão não encontrada!");
            else {
                if(!sessaoWeb.estaLogado())
                    System.out.println("Usuário não está logado!");
                else{
                    Leilao leilao = buscarLeilao(chaveLeilao, hibernateSession, transaction);
                    if (leilao == null)
                        System.out.println("Leilao não encontrado!");
                    else{
                        sessaoWeb.getUsuario().darLance(tipo.equals("Compra"), leilao, valor, valor);
                        sessaoWeb.setHoraUltimoAcesso(agora());
                        hibernateSession.save(sessaoWeb.getUsuario());
                        hibernateSession.save(sessaoWeb);
                    }
                }
            }
            
        transaction.commit();
        hibernateSession.close();
    }
    
    @WebMethod(operationName = "Solicitar_senha")
    public void solicitarSenha(@WebParam String email)
            throws UnsupportedEncodingException, NoSuchAlgorithmException{
        Session hibernateSession = NovoHibernateUtil.getSessionFactory().openSession();
        Transaction transaction = hibernateSession.beginTransaction();
        
        Usuario usuario = buscarUsuario(email, hibernateSession, transaction);
        
        if (usuario != null)
            try{
                String novaSenha = Criptografia.gerarSenha();
                String txt = "Olá, " + usuario.getNome();
                txt+= "\n\nComo solicitado recentemente ao Sistema de Leilões, enviamos sua nova senha:\n";
                txt+= "\t" + novaSenha;
                JavaMailApp.enviarEmail(usuario.getEmail(), "Sistema de Leilao - Recuperacao de senha", txt);
                usuario.setSenha(Criptografia.criptografia(novaSenha));
                System.out.println("Nova senha enviada por email.");
            }
            catch(MessagingException e){
               System.out.println(e.getMessage());
               System.out.println("Nao foi possivel solicitar nova senha!");
            }
        else
            System.out.println("Usuário nao encontrado!");
        
        hibernateSession.save(usuario);
        transaction.commit();
        
        hibernateSession.close();
    }
       
    @WebMethod(operationName = "Alterar_senha")
    public void alterarSenha(
            @WebParam(name = "chave_da_sessao") String chaveSessao, 
            @WebParam(name = "Senha_atual") String senhaAtual,
            @WebParam(name = "Nova_senha") String novaSenha1, 
            @WebParam(name = "Confirmar_nova_senha") String novaSenha2)
            throws UnsupportedEncodingException, NoSuchAlgorithmException{
        
        Session hibernateSession = NovoHibernateUtil.getSessionFactory().openSession();
        Transaction transaction = hibernateSession.beginTransaction();
            SessaoWeb sessaoWeb = buscarSessao(chaveSessao, hibernateSession, transaction);
            
            if (sessaoWeb == null)
                System.out.println("Chave da sessão não encontrada!");
            else {
                if(!sessaoWeb.estaLogado())
                    System.out.println("Usuário não está logado!");
                else{
                    Usuario usuario = sessaoWeb.getUsuario();
                    if(!Arrays.equals(usuario.getSenha(), Criptografia.criptografia(senhaAtual)))
                        System.out.println("Senha atual incorreta!");
                    else {
                        if(!novaSenha1.equals(novaSenha2))
                            System.out.println("Os campos de senha devem ser iguais!");
                        else {
                            usuario.setSenha(Criptografia.criptografia(novaSenha1));
                            hibernateSession.save(sessaoWeb);
                            System.out.println("Senha alterada!");
                        }
                    }
                }
            }
            sessaoWeb.setHoraUltimoAcesso(agora());
        transaction.commit();
        hibernateSession.close();
    }

    @WebMethod(operationName = "Fazer_login")
    public String logar(@WebParam(name = "email") String email,
            @WebParam(name = "senha") String senha)
            throws UnsupportedEncodingException, NoSuchAlgorithmException{
        
        System.out.println("Email:" + email + "\tSenha" + senha);
        String chave = null;
        
        Session hibernateSession = NovoHibernateUtil.getSessionFactory().openSession();
        Transaction transaction = hibernateSession.beginTransaction();
            Usuario usuario = buscarUsuario(email, hibernateSession, transaction);
            
            if (usuario != null && Arrays.equals(usuario.getSenha(), Criptografia.criptografia(senha) )){
                SessaoWeb sessaoWeb = new SessaoWeb(usuario);
                hibernateSession.save(sessaoWeb);
                transaction.commit();
                System.out.println("Login efetuado com sucesso às " + sessaoWeb.getHoraLogin().toString());
                chave = sessaoWeb.getChaveDaSessao();
            }else
                System.out.println("Nao foi possivel realizar o login!");
        hibernateSession.close();
        
        return chave;
    }
        
    /**
     * Retorna a hora atual (Timestamp)
     * @return 
     */
    private static Timestamp agora(){
        return new Timestamp(Calendar.getInstance().getTimeInMillis());
    }
     
    /*
    @WebMethod(operationName = "TestarSessao")
    public void TestarSessao(@WebParam(name = "email") String email){
        Session hibernateSession = NovoHibernateUtil.getSessionFactory().openSession();
            System.out.println("Tentando incluir " + email + " na sessão.");
            String chave = IncuirNaSessao(email);
            System.out.println("Tentando buscar " + chave + " na sessão.");
            Usuario usuario = BuscarNaSessao(chave);
        hibernateSession.close();
    }*/
    /*
    private String IncuirNaSessao(String email){
        Session hibernateSession = NovoHibernateUtil.getSessionFactory().getCurrentSession();
        Transaction transaction = hibernateSession.beginTransaction();
            Criteria busca = hibernateSession.createCriteria(Usuario.class);
            busca.add(Restrictions.eq("email", email));
            Object obj = busca.uniqueResult();
            Usuario usuario = (obj != null)? (Usuario) obj : null;
        
            SessaoWeb sessaoWeb = new SessaoWeb(usuario);
            hibernateSession.save(sessaoWeb);
        transaction.commit();
        
        System.out.println("Usuário " + sessaoWeb.getUsuario().getNome() + " incluido na sessão com chave:\n\t" + sessaoWeb.getChave());
        return sessaoWeb.getChave();
    }*/

    /*    
    private String incuirNaSessao(Usuario usuario, boolean hiberSessionOpened){
        Session hibernateSession = hiberSessionOpened ? NovoHibernateUtil.getSessionFactory().getCurrentSession() 
                                                      : NovoHibernateUtil.getSessionFactory().openSession();
        Transaction transaction = hibernateSession.beginTransaction();
            SessaoWeb sessaoWeb = new SessaoWeb(usuario);
            hibernateSession.save(sessaoWeb);
        transaction.commit();
        
        if (hiberSessionOpened)
            hibernateSession.close();
        
//        System.out.println("Usuário " + sessaoWeb.getUsuario().getNome() + " incluido na sessão com chave:\n\t" + sessaoWeb.getChave());
        return sessaoWeb.getChave();
    }*/
    /**
    ANTIGO MÓTODO USANDO HttpSession
    public String IncuirNaSessao(String email){
        Session hibernateSession = NovoHibernateUtil.getSessionFactory().openSession();
            Criteria busca = hibernateSession.createCriteria(Usuario.class);
            busca.add(Restrictions.eq("email", email));
            Object obj = busca.uniqueResult();
        hibernateSession.close();
        Usuario usuario = (obj != null)? (Usuario) obj : null;
        
        String chave = String.valueOf(UUID.randomUUID());
        
        MessageContext mc = wsContext.getMessageContext();
        HttpSession httpSession = ((javax.servlet.http.HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST)).getSession();
        System.out.println("sessao: " + httpSession);
        httpSession.setAttribute(chave, usuario);
        System.out.println("lista de atributos da sessao");
        for (Enumeration<String> e = httpSession.getAttributeNames(); e.hasMoreElements();)
            System.out.println('\t' + e.nextElement());
        System.out.println("Usuário " + usuario.getNome() + " incluido na sessão com chave:\n\t" + chave);
//        for (String e : sessao.keySet())
//            System.out.println('\t' + e);

        return chave;
    }
    */
    /*
    private boolean estaLogado(String chave, boolean hiberSessionOpened){
        Session hibernateSession = hiberSessionOpened ? NovoHibernateUtil.getSessionFactory().getCurrentSession() 
                                                      : NovoHibernateUtil.getSessionFactory().openSession();
            Transaction transaction = hibernateSession.beginTransaction();
            Criteria busca = hibernateSession.createCriteria(SessaoWeb.class);
            busca.add(Restrictions.eq("chave", chave));
            Object obj = busca.uniqueResult();  
            
            boolean logado = false;
            if (obj != null){
                SessaoWeb sessaoWeb = (SessaoWeb) obj;
                logado = sessaoWeb.estaLogado();
            }
        
        if (hiberSessionOpened)
            hibernateSession.close();
        
        return logado;
    }*/
}
