/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import org.hibernate.jpa.boot.spi.EntityManagerFactoryBuilder;

/**
 *
 * @author GET
 */
public class PersistenceUtil {
    public static EntityManager getEntityManager(){
        return  Persistence.createEntityManagerFactory("LeilaoPU").createEntityManager();
//        EntityManagerFactory factory = Persistence.createEntityManagerFactory("LeilaoPU");
//        System.out.println("---------------------- > FACTORY: " + factory);
//        return factory.createEntityManager();
    }
}
