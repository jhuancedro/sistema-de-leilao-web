/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

import java.util.List;
import javax.faces.bean.ManagedBean;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author jhcedro
 */
@ManagedBean /* TODO: verificar o tempo de vida */
/* Classe para gerenciar acessos ao banco atraves do Hibernate */
public class HibernateManager {
    /* TODO: Analisar uso de NovoHibernateUtil.getSessionFactory().abrirSessao();
        ou NovoHibernateUtil.getSessionFactory().getCurrentSession() e hibernateSession.close();
    */
    Session hibernateSession;

    public HibernateManager() {
        hibernateSession = NovoHibernateUtil.getSessionFactory().openSession();
        System.out.println("Sessao " + hibernateSession.toString() + "aberta");
//        transaction = this.hibernateSession.beginTransaction();
    }
    
    public void abrirSessao(){
        if(hibernateSession == null)
            hibernateSession = NovoHibernateUtil.getSessionFactory().openSession();
        else
            System.err.println("Uma sessao ja aberta!");
    }
    
    public void fecharSessao(){
        if(hibernateSession == null)
            System.err.println("Nenhuma sessao aberta!");
        else {
            hibernateSession.close();
            System.out.println("Sessao " + hibernateSession.toString() + "fechada");
            hibernateSession = null;            
        }
        
    }
    
    public void salvar(Object ...objects){
        Transaction transaction = hibernateSession.beginTransaction();

        for (Object object : objects) {
            if (!(object instanceof List)){
                System.out.println("SALVANDO: " + object.toString());
                hibernateSession.save(object);
            }
            else {
                for (Object o : (List<Object>) object) {
                    System.out.println("SALVANDO: " + o.toString());
                    hibernateSession.save(o);
                }
            }
        }
        
        transaction.commit();
    }
    
    /* Retorna o lista de objetos do tipo $tipo cujo campo $nomePropriedade eh $valor */
    public List<Object> buscarLista(Class tipo, String nomePropriedade, Object valor){        
        Criteria busca = hibernateSession.createCriteria(tipo);
        busca.add(Restrictions.eq(nomePropriedade, valor));
        List<Object> list =  busca.list();
        
        return list;
    }
    
    /* Retorna o unico objeto do tipo $tipo cujo campo $nomePropriedade eh $valor */
    public Object buscarUnico(Class tipo, String nomePropriedade, Object valor){        
        Criteria busca = hibernateSession.createCriteria(tipo);
        busca.add(Restrictions.eq(nomePropriedade, valor));
        
        Object object = busca.uniqueResult();
        
        return object;
    }
    
    /* Retorna o lista de objetos de um dado tipo*/
    public List<Object> listar(Class tipo){        
        Criteria busca = hibernateSession.createCriteria(tipo);
        List<Object> list = busca.list();
        
        return list;
    }
}
