/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilitarios;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

/**
 *
 * @author GET
 */
public class Criptografia {
    /**
     * Gera uma senha aleatória e oito (8) caracteres.
     * @return 
     */
    public static String gerarSenha(){
        return gerarSenha("", 8);   }
    
    /**
     * Gerar uma senha (sequencia de caracteres aleatorios).
     * @param inicio Inicial da sequência
     * @param tam Tamanho da sequencia gerada
     * @return sequencia de caracteres
     */
    public static String gerarSenha(String inicio, int tam){
        String senha = inicio;
        for (int cont= 0; cont<tam; cont++) {
                 /*
                *    matriz contendo em cada linha indices (inicial e final) da tabela ASCII para retornar alguns caracteres.
                *    [48, 57] = numeros;
                *    [64, 90] = "@" mais letras maiusculas;
                *    [97, 122] = letras minusculas;
                */
                int ascii[][] = {{48, 57}, {64,90}, {97,122}};
                int i = (int) Math.floor(Math.random()*ascii.length);
                senha += (char) ((int) Math.floor(Math.random()*(ascii[i][1]-ascii[i][0])) + ascii[i][0]);
        }
        return senha;
    }

    /**
     * Criptografia uma dada senha.
     * @param senha Senha a ser criptografada.
     * @return Senha criptografada com SHA-256.
     * @throws NoSuchAlgorithmException
     * @throws UnsupportedEncodingException 
     */
    public static byte[] criptografia(String senha) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        MessageDigest algorithm = MessageDigest.getInstance("SHA-256");
        return algorithm.digest(senha.getBytes("UTF-8"));
    }
    
    /**
     * Verifica se criptografia() de senha corresponde a senhaCript
     * @param senha
     * @param senhaCript
     * @return true se criptografia de senha é igual a senhaCript
     * @throws NoSuchAlgorithmException
     * @throws UnsupportedEncodingException 
     */
    public static boolean comparar(String senha, byte[] senhaCript) throws NoSuchAlgorithmException, UnsupportedEncodingException{
        return Arrays.equals(criptografia(senha), senhaCript);
    }
}
