package controllers;
 
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.mail.MessagingException;
import javax.naming.NamingException;
import modelo.Usuario;
import persistencia.HibernateManager;
import utilitarios.Criptografia;
import utilitarios.JavaMailApp;
 
@ManagedBean
public class CadastroController {
    private String nome;
    private String email;
    private String pais;    //TODO: Anexar lista de paises
    private String instituicao;
     
    public void cadastrar(){
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Efetuando cadastro..."));
//        Session hibernateSession = NovoHibernateUtil.getSessionFactory().abrirSessao();
//        Transaction transaction = hibernateSession.beginTransaction();
//        GenericDAO<Usuario> usuarioDAO = new GenericDAO<>(Usuario.class);
//        EntityManager manager = PersistenceUtil.getEntityManager();
//        Usuario usuario = buscarUsuario(email, hibernateSession, transaction);
//        Usuario usuario = usuarioDAO.listOnce("email", email, manager);
        HibernateManager hm = new HibernateManager();
        Object object = hm.buscarUnico(Usuario.class, "email", email);
        if( object != null)
//            System.out.println("Usuário já cadastrado!");       
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Usuário já cadastrado!"));
        else{
            // TODO: Conferir e incluir CPF
            Usuario novoUsuario = new Usuario(nome, email);
            try{
                String novaSenha = Criptografia.gerarSenha();
                String txt = "Olá, " + novoUsuario.getNome();
                txt+= "\n\nSeja bem-vindo ao Sistema Web de Leilões!\nAqui está a sua primeira senha:\n";
                txt+= "\t" + novaSenha;
//                FIXME: Envio de email desativado
                JavaMailApp.enviarEmail(novoUsuario.getEmail(), "Sistema de Leilao - Cadastro", txt);
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Primeira senha enviada por email!"));
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Primeira Senha: \""+novaSenha+"\"."));

                novoUsuario.setSenha(Criptografia.criptografia(novaSenha));
                hm.salvar(novoUsuario);
                System.out.println("Nova senha enviada por email.");
            }catch(MessagingException | NoSuchAlgorithmException | UnsupportedEncodingException e){
                System.out.println(e.getMessage());
                System.out.println("Nao foi possivel eftuar o cadastro!");
            }
        }
        hm.fecharSessao();
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the pais
     */
    public String getPais() {
        return pais;
    }

    /**
     * @param pais the pais to set
     */
    public void setPais(String pais) {
        this.pais = pais;
    }

    /**
     * @return the instituicao
     */
    public String getInstituicao() {
        return instituicao;
    }

    /**
     * @param instituicao the instituicao to set
     */
    public void setInstituicao(String instituicao) {
        this.instituicao = instituicao;
    }
}