package controllers;
 
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.naming.NamingException;
import modelo.Testes_Leilao;
import modelo.Usuario;
import org.hibernate.Session;
import persistencia.HibernateManager;
import utilitarios.Criptografia;
import webService.SessaoWeb;
 
@ManagedBean
public class LoginController{
    private String email;
    private String senha;
    private String link;
    
    public LoginController() {
    
    }
    
    public String logar() {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_WARN, "Erro ao logar", "Credenciais inválidas");;
        String link = "index.xhtml";
        
        HibernateManager hm = new HibernateManager();
        Object object = hm.buscarUnico(Usuario.class, "email", email);
        if (object != null){
            Usuario usuario = (Usuario) object;
            try {
                if(Arrays.equals(usuario.getSenha(), Criptografia.criptografia(senha))){
                    SessaoWeb sessaoWeb = new SessaoWeb(usuario);
                    hm.salvar(usuario, sessaoWeb);
                    message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Bem-vindo", usuario.getNome());
                    link = "home.xhtml";
                }
            } catch (NoSuchAlgorithmException | UnsupportedEncodingException ex) {
                Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        this.link = link;
        FacesContext.getCurrentInstance().addMessage(null, message);
        
        hm.fecharSessao();
        
        return link;
    }
    
    public void pontapeInicial() throws NamingException, SQLException{
        Testes_Leilao.main();
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the senha
     */
    public String getSenha() {
        return senha;
    }

    /**
     * @param senha the senha to set
     */
    public void setSenha(String senha) {
        this.senha = senha;
    }

    /**
     * @return the link
     */
    public String getLink() {
        return link;
    }

    /**
     * @param link the link to set
     */
    public void setLink(String link) {
        this.link = link;
    }
}