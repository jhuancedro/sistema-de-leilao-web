package controllers;
 
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
 
@ManagedBean
public class RecuperarSenhaController {
    private String email;
     
    public void recuperarSenha() {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Nova senha enviada por email..."));
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }
}