package controllers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import modelo.Leilao;
import persistencia.HibernateManager;

/**
 *
 * @author jhcedro
 */
@ManagedBean
public class HomeController implements Serializable{
    private List<String> mensagens;
    private List<String> convites;
    private List<Leilao> leiloes;
    
    @PostConstruct
    public void init(){
        mensagens = new ArrayList<String>();
        mensagens.add("Mensagem de usuário 1");
        
        convites = new ArrayList<String>();
        convites.add("Convite para leilão 1");
        convites.add("Convite para leilão 2");
        
        HibernateManager hm = new HibernateManager();
        Object object = hm.listar(Leilao.class);
        if(object != null){
            System.out.println("Eba!");
            leiloes = (ArrayList<Leilao>) object;
        }
        else
            leiloes = new ArrayList<Leilao>();
        hm.fecharSessao();
    }

    /**
     * @return the mensagens
     */
    public List<String> getMensagens() {
        return mensagens;
    }

    /**
     * @param mensagens the mensagens to set
     */
    public void setMensagens(List<String> mensagens) {
        this.mensagens = mensagens;
    }

    /**
     * @return the convites
     */
    public List<String> getConvites() {
        return convites;
    }

    /**
     * @param convites the convites to set
     */
    public void setConvites(List<String> convites) {
        this.convites = convites;
    }

    /**
     * @return the leiloes
     */
    public List<Leilao> getLeiloes() {
        return leiloes;
    }

    /**
     * @param leiloes the leiloes to set
     */
    public void setLeiloes(List<Leilao> leiloes) {
        this.leiloes = leiloes;
    }
    
    
}
