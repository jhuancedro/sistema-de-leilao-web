package modelo;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.naming.NamingException;
import persistencia.HibernateManager;
import utilitarios.Criptografia;

@ManagedBean(name="WEB")
public class Testes_Leilao {
    public static void main() throws NamingException, SQLException {
        /*  Removendo GenericDAO*/
//        GenericDAO<Usuario> usuarioDAO = new GenericDAO<>(Usuario.class);
//        EntityManager manager = PersistenceUtil.getEntityManager();
//        Session hibernateSession = NovoHibernateUtil.getSessionFactory().abrirSessao();
//        Transaction transaction = hibernateSession.beginTransaction();
/**/
        Leilao leilao = new Leilao("Leilão Pontapé");
        leilao.iniciar(60*1000);

        List<Usuario> usuarios = new ArrayList<>();
        usuarios.add( new Usuario("Adalberto", "adalberto@email.com") );
        usuarios.add( new Usuario("Algoz", "algoz@email.com") );
        usuarios.add( new Usuario("Andre", "andre@email.com") );
        usuarios.add( new Usuario("Antonia", "antonia@email.com") );
        usuarios.add( new Usuario("Arnaldo", "arnaldo@email.com") );
        usuarios.add( new Usuario("Atila", "atila@email.com") );
        usuarios.add( new Usuario("Jhuan", "jhuancedro@gmail.com") );

        List<Participacao> participacoes = new ArrayList<>();
        for (Usuario usuario : usuarios)
            participacoes.add(usuario.participarLeilao(leilao, 500));

        List<Lance> lances = new ArrayList<>();
        lances.add(usuarios.get(1).comprar(leilao, 500.0f, 1.0f));
        lances.add(usuarios.get(4).comprar(leilao,  30.0f, 1.0f));
        lances.add(usuarios.get(3).comprar(leilao, 320.0f, 1.0f));
        lances.add(usuarios.get(4).comprar(leilao, 200.0f, 1.0f));
        lances.add(usuarios.get(2).vender(leilao, 100.0f, 2.0f));
        lances.add(usuarios.get(2).vender(leilao,  80.0f, 1.0f));
        lances.add(usuarios.get(5).vender(leilao, 150.0f, 2.3f));
        lances.add(usuarios.get(1).vender(leilao, 100.0f, 1.7f));
        lances.add(usuarios.get(0).vender(leilao, 150.0f, 0.9f));

//        Session hibernateSession = NovoHibernateUtil.getSessionFactory().abrirSessao();
//        Transaction transaction = hibernateSession.beginTransaction();
        // Salvando
        
//        for (Lance lance : lances) {
//            hibernateSession.save(lance);
//        }
//        for (Participacao participacao : participacoes) {
//            hibernateSession.save(participacao);
//        }
//        for (Usuario usuario : usuarios){
//            System.out.println("---------- HEY ------------");
//            hibernateSession.save(usuario);
//        }
//        hibernateSession.save(leilao);
//        
//        transaction.commit();
//        hibernateSession.close();

    Usuario admin = new Usuario("admin", "admin");
    try {
        admin.setSenha(Criptografia.criptografia("admin"));
    } catch (NoSuchAlgorithmException | UnsupportedEncodingException ex) {
        Logger.getLogger(Testes_Leilao.class.getName()).log(Level.SEVERE, null, ex);
    }
    usuarios.add( admin );
    
    HibernateManager hm = new HibernateManager();
    hm.salvar(leilao, lances, participacoes, usuarios);
    Object j = hm.buscarUnico(Usuario.class, "nome", "Jhuan");
    if (j!=null)
            System.out.println(((Usuario) j).getEmail());
    hm.fecharSessao();
    /**/
    
    }
}
