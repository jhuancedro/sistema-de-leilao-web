package modelo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Participacao implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    
    @ManyToOne//(cascade = CascadeType.PERSIST)
    private Usuario usuario;
    
    @ManyToOne//(cascade = CascadeType.ALL)
    private Leilao leilao;
    
    @Column
    private float estoqueInicial;

    @OneToMany(mappedBy = "participacao")
    private List<Lance> lances;
    
    public Participacao() {
        lances = new ArrayList<>();
    }

    public Participacao(Usuario usuario, Leilao leilao, float estoqueInicial) {
        this.usuario = usuario;
        this.leilao = leilao;
        this.estoqueInicial = estoqueInicial;
        lances = new ArrayList<>();
    }
    
    public void adicionarLance(Lance lance){
        this.lances.add(lance);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Leilao getLeilao() {
        return leilao;
    }

    public void setLeilao(Leilao leilao) {
        this.leilao = leilao;
    }

    public float getEstoqueInicial() {
        return estoqueInicial;
    }

    public void setEstoqueInicial(float estoqueInicial) {
        this.estoqueInicial = estoqueInicial;
    }

    public List<Lance> getLances() {
        return lances;
    }

    public void setLances(List<Lance> lances) {
        this.lances = lances;
    }
}
